<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Http\Resources\UserResource;
use App\Http\Controllers\UserController;
use Auth;
use DB;

class AuthController extends Controller
{
    //
    public function login(Request $request){
        $credentials = $request->only('email', 'password');
        if(Auth::attempt($credentials)){
            $user = Auth::user();
            $token = $user->createToken('MyApp')->accessToken;

            return response()->json([
                "message" => "Login Concluído",
                "user"    => $user,
                "token"   => $token
            ], 200);
        }
        else{
            return response()->json([
                "message" => "E-mail ou senha inválidos."
            ], 500);
        }    
    }

    public function logout(){
        $accessToken = Auth::user()->token();
        DB::table('oauth_refresh_tokens')->where('access_token_id', $accessToken->id)->update(['revoked'=>'true']);
        $accessToken->revoke();
        return response()->json(["User deslogado."], 200);
    }

    public function register(Request $request)
    {
        $newuser = new User();
        $newuser->createUser($request);
        $success['token'] = $newuser->createToken('MyApp')->accessToken;
        $newuser->save();
        return response()->json(['success' => $success, 'user' => $newuser], 200);

    }

}
