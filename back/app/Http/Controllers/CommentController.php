<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Http\Requests\CommentRequest;
use Auth;

class CommentController extends Controller
{
    //
    public function create(CommentRequest $request)
    {
        $user = Auth::user();
        $request->user_id = $user->id;
        $comment = new Comment();
        $comment->createComment($request,$user->id);
        return response()->json(['comment'=> $comment], 200);
    }
    public function update (CommentRequest $request, $id){
        $comment = Comment::find($id);
        $comment->updateComment($request);
        return response()->json(['comment' => $comment], 200);
    }
    public function index(){
        $comments = Comment::all();
        return response()->json(['comment' => $comments],200);
    }

    public function show($id){
        $comment = Comment::find($id);
        return response()->json(['comment'=> $comment], 200);
    }

    public function destroy($id){
        $comment = Comment::find($id);
        $comment->delete();
        return response()->json(['Comentário deletado com sucesso'],200);
    }
}
