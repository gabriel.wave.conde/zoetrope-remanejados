<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Review;
use App\Models\Comment;

class AuthAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $tipo)
    {
        $user = Auth::User();
        if($tipo == 'comment'){
            $comment = Comment::find($request->id);
            if($user->id == $comment->user_id || $user->is_admin == 1){
                return $next($request);
            }
            else{
                return response()->json(['Não autorizado'],401);
            }
        }
        if($tipo == 'review'){
            $review = Review::find($request->id);
            if($user->id == $review->user_id || $user->is_admin == 1){
                return $next($request);
            }
            else{
                return response()->json(['Não autorizado'],401);
            }
        }
        if($tipo == 'film'){
            $film = Film::find($request->id);
            if($user->is_admin == 1){
                return $next($request);
            }
            else{
                return response()->json(['Não autorizado'],401);
            }
        }
        if($tipo == 'admin'){
            if($user->is_admin == 1 || $user->id == $request->id){
                return $next($request);
            }
            else{
                return response()->json(['Não autorizado'],401);
            }
        }
    }    
    
}