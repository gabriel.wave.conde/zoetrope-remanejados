<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Moldels\Film;

class FilmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('POST')){
            return [
                'age_rating' => 'required|integer',
                'director' => 'required|string',
                'genre' => 'required|string',
                'length' => 'required|string',
                'film_rating' => 'required|numeric',
                'release_date' => 'required|dateformat:d/m/Y',
                'resume' => 'required|longText',
                'title' => 'required|string'
            ];
        }
        if ($this->isMethod('PUT')){
            return [
                'age_rating' => 'integer',
                'director' => 'string',
                'genre' => 'string',
                'length' => 'string',
                'film_rating' => 'numeric',
                'release_date' => 'dateformat:d/m/Y',
                'resume' => 'longText',
                'title' => 'string'
            ];
        }
    }
    public function message()
    {
        return[
            'age_rating.required' => 'este atributo não pode ser nulo',
            'director.required' => 'este atributo não pode ser nulo',
            'genre.required' => 'este atributo não pode ser nulo',
            'length.required' => 'este atributo não pode ser nulo',
            'film_rating.required' => 'este atributo não pode ser nulo',
            'release_date.required' => 'este atributo não pode ser nulo',
            'resume.required' => 'este atributo não pode ser nulo',
            'title.required' => ' este atributo não pode ser nulo'
        ];
    }
}
