<?php

namespace Database\Factories;

use App\Models\Film;
use Illuminate\Database\Eloquent\Factories\Factory;

class FilmFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Film::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'title' => $this->faker->sentence($nbWords=5, $variableNbWords = true),
            'director' => $this->faker->name,
            'film_rating' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max=5),
            'film_cover' => 'https://picsum.photos/182/268'
        ];
    }
}
