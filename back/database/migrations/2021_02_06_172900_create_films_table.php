<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->id();
            $table->integer('age_rating')->nullable()->default(null);
            $table->string('director');
            $table->string('genre')->default(null)->nullable();
            $table->string('length')->default(null)->nullable(); 
            $table->float('film_rating');
            $table->string('film_cover')->default(null)->nullable();
            $table->string('film_scene')->default(null)->nullable();
            $table->string('release_date')->default(null)->nullable();
            $table->longText('resume')->default(null)->nullable();
            $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
