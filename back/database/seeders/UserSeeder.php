<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Review;
use App\Models\Comment;
use App\Models\Film;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        // DB::table('users')->insert([
        //     'name' => 'guilherme',
        //     'email' => 'ggomes.ufrj@gmail.com',
        //     'password' => 'senha123'
        // ]);


        User::factory()->count(5)->create()->each(function($user){
            Film::factory()->count(3)->create()->each(function($film)use ($user){
                $reviews = Review::factory()->count(2)->create([
                    'user_id' => $user->id,
                    'film_id' => $film->id
                ])->each(function($review)use($user, $film){
                    $comment = Comment::factory()->count(2)->create([
                        'user_id'=>$user->id,
                        'review_id'=>$review->id
                    ]);
                });
            });
        });
        $lista_title =      ['Velozes & Furiosos 8','Your Name.','Cinco Centímetros por Segundo','The Mortuary Collection',
                             'Interestelar','12 Horas para Sobreviver: O Ano da Eleição','A nova onda do imperador',
                             'A Saga Crepúsculo: Amanhecer - Parte 1','Vingadores: Ultimato','Karatê Kid',
                             'Alguém Avisa?','Forrest Gump','Circle','Fragmentado','Deadpool' ];

        $lista_genre =      ['Ação', 'Animação', 'Animação', 'Horror', 'Aventura', 'Ação', 'Animação', 'Fantasia',
                             'Ação', 'Ação', 'Comédia', 'Drama', 'Drama', 'Horror', 'Ação'];

        $lista_film_rating= ['3.4', '4.2', '3.8', '3.3', '4.3', '3.0', '3.7', '2.5',
                             '4.2', '3.1', '3.4', '4.4', '3.0', '4.7', '4.0'];

        $lista_film_cover=  ['https://m.media-amazon.com/images/M/MV5BMjMxODI2NDM5Nl5BMl5BanBnXkFtZTgwNjgzOTk1MTI@._V1_UX182_CR0,0,182,268_AL_.jpg',
                             'https://cdna.artstation.com/p/assets/images/images/023/468/228/large/leandro-raimundo-your-name-fan-poster.jpg?1579290360',
                             'http://mcswebsites.blob.core.windows.net/1033/Event_8415/gallery/5smPoster.jpg',
                             'https://m.media-amazon.com/images/M/MV5BOTJkNGZkYjEtNDdjMy00NDYxLWE1MDYtNTgyMWNhOWRhZTBkXkEyXkFqcGdeQXVyNDc0MzY5MTk@._V1_UX182_CR0,0,182,268_AL_.jpg',
                             'https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg',
                             'https://m.media-amazon.com/images/M/MV5BMjI3MDI0MTA1N15BMl5BanBnXkFtZTgwOTk4NjU5ODE@._V1_UX182_CR0,0,182,268_AL_.jpg',
                             'https://m.media-amazon.com/images/M/MV5BZGQwNmFhNjctNzQ0Yy00ZmE5LWIyMTEtYTZhZWQ4OTFmNDI2L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNTMxMjgxMzA@._V1_UX182_CR0,0,182,268_AL_.jpg',
                             'https://m.media-amazon.com/images/M/MV5BODgxNDE0OTAzOF5BMl5BanBnXkFtZTcwNzcwODE2Ng@@._V1_UX182_CR0,0,182,268_AL_.jpg',
                             'https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_UX182_CR0,0,182,268_AL_.jpg',
                             'https://m.media-amazon.com/images/M/MV5BYjQ1NzRhYjYtMWY3My00ODA0LTk5ZDctYjQ4YjE0M2RhMGNiXkEyXkFqcGdeQXVyNTUyMzE4Mzg@._V1_UX182_CR0,0,182,268_AL_.jpg',
                             'https://m.media-amazon.com/images/M/MV5BZDgyZDNiZGEtYmQ4NC00NzU5LTllODQtNjIwMGY2NDYyNGQwXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg',
                             'https://m.media-amazon.com/images/M/MV5BNWIwODRlZTUtY2U3ZS00Yzg1LWJhNzYtMmZiYmEyNmU1NjMzXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UY268_CR1,0,182,268_AL_.jpg',
                             'https://m.media-amazon.com/images/M/MV5BMTg0Mjg0MjUwOF5BMl5BanBnXkFtZTgwNjk0NzQ4NzE@._V1_UX182_CR0,0,182,268_AL_.jpg',
                             'https://m.media-amazon.com/images/M/MV5BZTJiNGM2NjItNDRiYy00ZjY0LTgwNTItZDBmZGRlODQ4YThkL2ltYWdlXkEyXkFqcGdeQXVyMjY5ODI4NDk@._V1_UX182_CR0,0,182,268_AL_.jpg',
                             'https://m.media-amazon.com/images/M/MV5BYzE5MjY1ZDgtMTkyNC00MTMyLThhMjAtZGI5OTE1NzFlZGJjXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg'
                            ];

        $lista_film_scene=  ['https://ultimateactionmovies.com/wp-content/uploads/2020/05/The-Fate-of-the-Furious.jpeg',
                             'https://i1.wp.com/aftercredits.com/wp-content/uploads/2017/04/YourNameFeat.jpg?fit=1200%2C675',
                             'https://miro.medium.com/max/3840/1*Pb8sUUJaqyq4Xip-NQ3cAQ.jpeg',
                             'https://blasphemoustomes.com/wp-content/uploads/2020/10/the-mortuary-collection-1-1024x431.png',
                             'https://api.time.com/wp-content/uploads/2014/10/interstellar-04.jpg',
                             'https://media.npr.org/assets/img/2016/06/27/the-purge_wide-c0a1f40389f28dd89aa9b020a9ef598db0534e4e.jpg',
                             'https://houseofgeekery.files.wordpress.com/2015/04/puffs2.png',
                             'https://fazewp-fazemediainc.netdna-ssl.com/cms/wp-content/uploads/2017/03/breaking-dawn-part-1-2-600x300.jpg',
                             'https://www.gannett-cdn.com/presto/2018/12/07/USAT/7b61e4bf-00b0-465d-9d71-02fe6b89698b-VPCTRAILER_AVENGERS_ENDGAME_DESK_THUMB.jpg',
                             'https://static01.nyt.com/images/2010/06/11/movies/11karatespan-1/KARATE-articleLarge.jpg?quality=75&auto=webp&disable=upscale',
                             'https://static1.colliderimages.com/wordpress/wp-content/uploads/2020/11/happiest-season-hulu-mackenzie-davis-harper-social-featured.jpg?q=50&fit=contain&w=943&h=472&dpr=1.5',
                             'https://the-take.com/images/uploads/screenprism/_constrain-1080w/Kids.jpg',
                             'http://4.bp.blogspot.com/-d3ywtLARw4Y/Vp074N6doZI/AAAAAAAACiY/7KCm60zJSE4/s1600/Cirlc.JPG',
                             'https://www.denofgeek.com/wp-content/uploads/2017/01/split_ending_anya_taylor_joy.jpg',
                             'https://i.ytimg.com/vi/nQUdOmRj6lk/maxresdefault.jpg'];

        $lista_release_date=['2017','2016','2007','2019','2014','2016','2000','2011','2019','2010','2020','1994',
                             '2015','2016','2016'];

        $lista_resume      =['Depois que Brian e Mia se aposentaram, e o resto da equipe foi exonerado, Dom e Letty estão em lua de mel e levam uma vida pacata e completamente normal. Mas a adrenalina do passado volta com tudo quando uma mulher misteriosa faz com que Dom retorne ao mundo do crime e da velocidade.',
                             'Mitsuha é a filha do prefeito de uma pequena cidade, mas sonha em tentar a sorte em Tóquio. Taki trabalha em um restaurante em Tóquio e deseja largar o seu emprego. Os dois não se conhecem, mas estão conectados pelas imagens de seus sonhos.',
                             '5 centímetros por segundo é um drama romântico que segue o caminho de Takaki Tohno e Akari Shinohara. A história se desenrola em três episódios em diferentes momentos de suas vidas: um encontro (o que parecia ser) fatídico e uma amizade inseparável se transforma em amor genuíno. Uma separação cruel e fora de controle levou a um desejo e a uma promessa que não pôde ser cumprida. Assim, este evento foi a causa de uma vida cheia de arrependimentos e solidão insuportável.',
                             'Prestes a se aposentar, um excêntrico agente funerário conta várias das histórias mais estranhas vividas em sua longa carreira. As coisas mudam para o fantasmagórico quando ele descobre que a história final é a sua.',
                             'As reservas naturais da Terra estão chegando ao fim e um grupo de astronautas recebe a missão de verificar possíveis planetas para receberem a população mundial, possibilitando a continuação da espécie. Cooper é chamado para liderar o grupo e aceita a missão sabendo que pode nunca mais ver os filhos. Ao lado de Brand, Jenkins e Doyle, ele seguirá em busca de um novo lar.',
                             'Em época de eleições, o policial Barnes precisa proteger a senadora Charlie Roan de uma noite anual de crimes, onde todos os delitos são permitidos por 12 horas.',
                             'O jovem e arrogante Imperador Kuzco é transformado em uma lhama por sua poderosa mentora chamada Yzma. Perdido na floresta, a única chance de Kuzco recuperar seu trono é com a ajuda de Pacha, um humilde camponês. Juntos, eles precisam enfrentar a bruxa Yzma antes de concluir sua jornada.',
                             'O casamento de Edward e Bella, sua lua de mel e o nascimento do filho desencadeiam uma série de acontecimentos que trará desdobramentos chocantes para Jacob.',
                             'Após Thanos eliminar metade das criaturas vivas, os Vingadores têm de lidar com a perda de amigos e entes queridos. Com Tony Stark vagando perdido no espaço sem água e comida, Steve Rogers e Natasha Romanov lideram a resistência contra o titã louco.',
                             'Um garoto de 12 anos chamado Dre Parker se muda para a China com a mãe e se vê em um terra estranha. Ele sabe um pouco de caratê, mas suas habilidades não são o bastante para enfrentar o valentão da escola, Cheng. Dre faz amizade com o Sr. Han, um mestre das artes marciais, que lhe ensina os segredos do kung fu na esperança de que o garoto possa derrotar Cheng e, quem sabe, conquistar o coração da linda Mei Ying.',
                             'As namoradas Harper e Abby visitam a família de Harper para o jantar anual de Natal. No entanto, logo após chegar, Abby percebe que a moça tem mantido seu relacionamento em segredo de seus pais conservadores.',
                             'Mesmo com o raciocínio lento, Forrest Gump nunca se sentiu desfavorecido. Graças ao apoio da mãe, ele teve uma vida normal. Seja no campo de futebol como um astro do esporte, lutando no Vietnã ou como capitão de um barco de pesca de camarão, Forrest inspira a todos com seu otimismo. Mas a pessoa que Forrest mais ama pode ser a mais difícil de salvar: seu amor de infância, a doce e perturbada Jenny.',
                             'Um grupo de 50 estranhos aguarda sua execução e recebe a difícil tarefa de escolher uma única pessoa dentre eles para escapar desse destino.',
                             'Kevin possui 23 personalidades distintas e consegue alterná-las quimicamente em seu organismo apenas com a força do pensamento. Um dia, ele sequestra três adolescentes que encontra em um estacionamento. Vivendo em cativeiro, elas passam a conhecer as diferentes facetas de Kevin e precisam encontrar algum meio de escapar.',
                             'Wade Wilson é um ex-agente especial que passou a trabalhar como mercenário. Seu mundo é destruído quando um cientista maligno o tortura e o desfigura completamente. O experimento brutal transforma Wade em Deadpool, que ganha poderes especiais de cura e uma força aprimorada. Com a ajuda de aliados poderosos e um senso de humor mais desbocado e cínico do que nunca, o irreverente anti-herói usa habilidades e métodos violentos para se vingar do homem que quase acabou com a sua vida.'
                            ];

        $lista_length       =['136','106','63','111','169','108','78','117','181','140','102','142','87','117','108'];

        $lista_director     =['F. Gary Gray', 'Makoto Shinkai', 'Makoto Shinkai', 'Ryan Spindell', 'Christopher Nolan',
                              'James DeMonaco', 'Mark Dindal', 'Bill Condon', 'Anthony Russo', 'Harald Zwart', 'Clea DuVall',
                              'Robert Zemeckis', 'Aaron Hann', 'M. Night Shyamalan', 'Tim Miller'];
        for($i = 0;$i<15;$i++){
            $film = Film::find($i+1);
            $film->title = $lista_title[$i];
            $film->director = $lista_director[$i];
            $film->length = $lista_length[$i];
            $film->resume = $lista_resume[$i];
            $film->release_date = $lista_release_date[$i];
            $film->film_scene = $lista_film_scene[$i];
            $film->film_cover = $lista_film_cover[$i];
            $film->genre = $lista_genre[$i];
            $film->film_rating = $lista_film_rating[$i];

            $film->save();
        };
        //$lista_filmes->save();
    }
}
