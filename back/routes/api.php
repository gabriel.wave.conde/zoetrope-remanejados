<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



//FILM ROUTES{

Route::get('film',[FilmController::class,'index']);
Route::get('film/{id}',[FilmController::class,'show']);
Route::put('film/film_by_title',[FilmController::class,'searchByName']);
Route::put('search',[FilmController::class,'searchBothByName']);
Route::get('film/reviews/{id}',[FilmController::class,'reviewsByFilm']);
Route::get('random_films',[FilmController::class,'randomFilms']);



//}


//REVIEW ROUTES{

Route::get('review',[ReviewController::class,'index']);
Route::get('review/{id}',[ReviewController::class,'show']);
Route::get('review/comments/{id}',[ReviewController::class,'commentsByReview']);
//MIDDLEWARE DE MOD OU O PRÓPRIO
//MIDDLEWARE DE MOD OU O PRÓPRIO

//USER ROUTES{

Route::get('user',[UserController::class,'index']);
Route::get('user/{id}',[UserController::class,'show']);
Route::get('user/reviews/{id}',[UserController::class,'usersReviews']);
Route::get('user/comments/{id}',[UserController::class,'usersComments']);
Route::post('user',[UserController::class,'create']);
Route::put('user/user_by_name',[UserController::class,'searchByName']);
Route::get('lastLikes/{id}',[UserController::class,'lastLikes']);
Route::get('avgRating/{id}',[UserController::class,'avgRating']);
//MIDDLEWARE DE MOD OU O PRÓPRIO
//MIDDLEWARE DE MOD OU O PRÓPRIO
Route::post('auth/login',[UserController::class,'login']);


//}

//COMMENT ROUTES{

Route::get('comment',[CommentController::class,'index']);
Route::get('comment/{id}',[CommentController::class,'show']);
//MIDDLEWARE DE MOD OU O PRÓPRIO
//MIDDLEWARE DE MOD OU O PRÓPRIO

//}

//AUTH ROUTES{
Route::post('auth/login',[AuthController::class,'login']);
Route::post('auth/register',[AuthController::class,'register']);
Route::group(['middleware' => 'auth:api'], function(){
    Route::get('auth/logout',[AuthController::class,'logout']);
    //USER
    Route::get('friend_list',[UserController::class,'friendList']);
    Route::get('liked_films',[UserController::class,'likedFilms']);
    Route::get('liked_reviews',[UserController::class,'likedReviews']);
    Route::get('followers',[UserController::class,'followers']);
    Route::get('is_following/{id}',[UserController::class,'isFollowing']);
    Route::post('user/follow/{id}',[UserController::class,'follow']);
    Route::post('user/like_film/{id}',[UserController::class,'likeFilm']);
    Route::post('user/like_review/{id}',[UserController::class,'likeReview']);
    Route::get('feed',[UserController::class,'feed']);
    Route::get('getDetails',[UserController::class,'getDetails']);
    //UPDATE
    Route::put('user',[UserController::class,'update']);
    //essa em especial o admin e o proprio usuario podem fazer caso seja o proprio perfil
    Route::delete('user',[UserController::class,'destroy'])->middleware('admin:admin');

    //FILM
    Route::get('is_liked_film/{id}',[FilmController::class,'isLiked']);
    Route::post('film',[FilmController::class,'create'])->middleware('admin:film');
    Route::put('film/{id}',[FilmController::class,'update'])->middleware('admin:film');
    Route::delete('film/{id}',[FilmController::class,'destroy'])->middleware('admin:film');



    //REVIEW
    Route::get('is_liked_review/{id}',[ReviewController::class,'isLiked']);
    Route::get('n_likes/{id}',[ReviewController::class,'nLikes']);
    Route::post('review',[ReviewController::class,'create']);
    Route::put('review/{id}',[ReviewController::class,'update'])->middleware('admin:review');
    Route::delete('review/{id}',[ReviewController::class,'destroy'])->middleware('admin:review');
    Route::get('review/n_likes/{id}',[ReviewController::class,'nLikes']);


    //COMMENT
    Route::post('comment',[CommentController::class,'create']);
    Route::put('comment/{id}',[CommentController::class,'update'])->middleware('admin:comment');
    Route::delete('comment/{id}',[CommentController::class,'destroy'])->middleware('admin:comment');
});

//}
