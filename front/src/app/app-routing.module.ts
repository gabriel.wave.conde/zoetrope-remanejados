import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../app/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'edit-profile/:id',
    loadChildren: () => import('./edit-profile/edit-profile.module').then( m => m.EditProfilePageModule),canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'film/:id',
    loadChildren: () => import('./film/film.module').then( m => m.FilmPageModule)
  },
   {
    path: 'friendlist/:id',
    loadChildren: () => import('./friendlist/friendlist.module').then( m => m.FriendlistPageModule), canActivate: [AuthGuard]
  },
  {
    path: 'comments/:id',
    loadChildren: () => import('./comments/comments.module').then( m => m.CommentsPageModule)
  },
  {
    path: 'create-review/:id',
    loadChildren: () => import('./create-review/create-review.module').then( m => m.CreateReviewPageModule),canActivate: [AuthGuard]
  },
  {
    path: 'create-comment/:id',
    loadChildren: () => import('./create-comment/create-comment.module').then( m => m.CreateCommentPageModule),canActivate: [AuthGuard]
  },
  {
    path: 'edit-review/:id',
    loadChildren: () => import('./edit-review/edit-review.module').then( m => m.EditReviewPageModule),canActivate: [AuthGuard]
  },
  {
    path: 'edit-comment/:id',
    loadChildren: () => import('./edit-comment/edit-comment.module').then( m => m.EditCommentPageModule)
  },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
