import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CommentsPageRoutingModule } from './comments-routing.module';

import { CommentsPage } from './comments.page';
import { UserCommentComponent } from '../components/user-comment/user-comment.component';
import { EditPostComponent } from '../components/edit-post/edit-post.component';
import { DeletePostComponent } from '../components/delete-post/delete-post.component';
import { LikeComponent } from '../components/like/like.component';
import { AddComponent } from '../components/add/add.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CommentsPageRoutingModule
  ],
  declarations: [CommentsPage, UserCommentComponent, EditPostComponent, DeletePostComponent, LikeComponent, AddComponent]
})
export class CommentsPageModule {}
