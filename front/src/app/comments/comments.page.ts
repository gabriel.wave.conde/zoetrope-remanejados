import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ReviewService } from '../services/review/review.service';
import { UserService } from '../services/user/user.service';
import { CommentService } from '../services/comment/comment.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.page.html',
  styleUrls: ['./comments.page.scss'],
})
export class CommentsPage implements OnInit {
  id:number;
  review;
  comments;
  user;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private reviewService: ReviewService, private userService: UserService, private commentService: CommentService) {
    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    })
  }

  go(){
    this.router.navigate(['/create-comment/' + this.id])
  }

  ngOnInit() {
    this.getReview(this.id);
    this.listReviewComments(this.id);
    this.getDetails();
  }

  getReview(id) {
    this.reviewService.getReview(id).subscribe(
      (res) => {
        this.review = res[0];
        console.log(this.review);
      }
    )
  }

  listReviewComments(id) {
    this.reviewService.listReviewComments(id).subscribe(
      (res) => {
        this.comments = res.reverse();
        console.log(this.comments);
      }
    )
  }

  getDetails(){
    if (localStorage.getItem('userToken')){
      this.userService.getDetails().subscribe(
        (res) => {
          this.user = res.user;
        }
      )
    }
  }

  deleteComment(id,i){
    this.commentService.deleteComment(id).subscribe(
      (res) => {
        console.log(res);
        this.comments.splice(i,1);
      }
    )
  }
}
