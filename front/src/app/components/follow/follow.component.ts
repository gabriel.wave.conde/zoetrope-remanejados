import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-follow',
  templateUrl: './follow.component.html',
  styleUrls: ['./follow.component.scss'],
})


export class FollowComponent implements OnInit {
  @Input() id
  buttonText: string = "Seguir";
  isClick: boolean = false;
  following;

  constructor(private userService: UserService) { 
  }



  ngOnInit() { 

    this.isFollowing();

  }

  changeColor() {
    this.follow();
  }

  changeText() {
    if(this.following) {
      this.buttonText = "Seguindo";
    } else {
      this.buttonText = "Seguir";
    }
  }

  isFollowing(){
    this.userService.isFollowing(this.id).subscribe(
      (res) => {
        this.following = res.is_following;
        console.log('fulana', this.following);
        this.changeText();
        
      }
    )
  }

  follow(){
    this.following = !this.following;
    this.changeText();
    this.userService.follow(this.id).subscribe(
      (res) =>{
        console.log(res);
      }
    );
    
  }
}
