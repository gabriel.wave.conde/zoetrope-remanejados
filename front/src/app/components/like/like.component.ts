import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ReviewService } from '../../services/review/review.service'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.scss'],
})
export class LikeComponent implements OnInit {
  @Output() clickOnFav = new EventEmitter<string>();
  @Input() id: number;
  @Input() type: string;
  isLiked;


  constructor(private reviewService: ReviewService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.isLikedReview();

  }

  public addFavorite() {
   // this.clickOnFav.emit('');
  }

  public toFav() {
    this.isLiked = !this.isLiked;
  }

  likeReview(){
    this.reviewService.likeReview(this.id).subscribe(
      (res) => {
        this.toFav();
        console.log(res);
      }
    )
  }

  isLikedReview(){
    this.reviewService.isLikedReview(this.id).subscribe(
      (res) => {
        this.isLiked = res.is_liked;
        console.log('plo', res);
      }
    )
  }

}
