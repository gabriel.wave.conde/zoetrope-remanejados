import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-comment',
  templateUrl: './user-comment.component.html',
  styleUrls: ['./user-comment.component.scss'],
})

export class UserCommentComponent implements OnInit {
  @Input() comment;

  constructor() { }

  ngOnInit() {}

}
