import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular'; 
import { CommentService } from '../services/comment/comment.service'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.page.html',
  styleUrls: ['./create-comment.page.scss'],
})
export class CreateCommentPage implements OnInit {

  addCommentForm: FormGroup;
  id: number;

  constructor(private router: Router, public formbuilder: FormBuilder, public toastCtrl: ToastController, private commentService: CommentService, private activatedRoute: ActivatedRoute, ) {
    this.addCommentForm = this.formbuilder.group({
      text: [null, [Validators.required, Validators.maxLength(140)]],
    })

    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    })
   }

  ngOnInit() {
  }

  addComment(form) {
    this.addCommentForm.value.review_id = this.id;
    console.log(form);
    console.log(form.value);
    this.commentService.createComment(form.value).subscribe(
      (res) => {
      console.log('ok', res);
      this.router.navigate(['/comments/' + this.id]).then(()=> {window.location.reload()});
      }
    )
  }

  async openToast() {  
    const toast = await this.toastCtrl.create({  
      message: 'Comentário enviado',   
      duration: 4000,
      position: 'middle',
      color: 'roxinho',
    });  
    toast.present();  
  }  
}