import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular'; 
import { ReviewService } from '../services/review/review.service'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-review',
  templateUrl: './create-review.page.html',
  styleUrls: ['./create-review.page.scss'],
})
export class CreateReviewPage implements OnInit {

  addReviewForm: FormGroup;
  id:number;

  constructor(private router: Router, public formbuilder: FormBuilder, public toastCtrl: ToastController, private reviewService: ReviewService, private activatedRoute: ActivatedRoute) {
    this.addReviewForm = this.formbuilder.group({
      text: [null, [Validators.required, Validators.maxLength(140)]],
      film_rating: [3],
    });

    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    })
  }


  ngOnInit() {
  }

  logRatingChange(rating){
    console.log("changed rating: ",rating);
  }

  addReview(form) {
    form.value.film_id = this.id;
    console.log(form);
    console.log(form.value);
    this.reviewService.createReview(form.value).subscribe(
      (res) => {
      console.log('ok', res);
      this.router.navigate(['/film/' + this.id]).then(()=> {window.location.reload()});
      }
    )
  }

  async openToast() {  
    const toast = await this.toastCtrl.create({  
      message: 'Review enviado',   
      duration: 4000,
      position: 'middle',
      color: 'roxinho',
    });  
    toast.present();  
  }  


}
