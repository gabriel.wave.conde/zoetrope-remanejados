import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth/auth.service';
import { ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user/user.service';


@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  editProfileForm: FormGroup;
  id: number;

  constructor(public formbuilder: FormBuilder, public authService: AuthService, public toastCtrl: ToastController, private router: Router, private userService: UserService, private activatedRoute: ActivatedRoute) {
    this.editProfileForm = this.formbuilder.group({
      user_name: [null, [Validators.minLength(3), Validators.maxLength(15)]],
      name: [null, [Validators.maxLength(20)]],
      email:[null, [Validators.email]],
      user_cover:[null,],
      user_avatar:[null,],
      password:[null, [Validators.minLength(6), Validators.maxLength(15)]],
  });

    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    })
  }

  ngOnInit() {
  }

  logout(){
    this.authService.logout().subscribe((res) => {
      console.log(res);
      localStorage.removeItem('userToken')
      this.router.navigate(['/tabs/tab5']).then(()=> {window.location.reload()});
    })
  }

  submitForm(){
    console.log(this.editProfileForm);

    this.userService.editProfile(this.editProfileForm.value).subscribe(
      (res) => {
      this.go();
      console.log(res);
      }
    )
  }

  go(){
    this.router.navigate(['/tabs/tab3/' + this.id]).then(()=> {window.location.reload()})
  }
}


