import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { ReviewService } from '../services/review/review.service'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-review',
  templateUrl: './edit-review.page.html',
  styleUrls: ['./edit-review.page.scss'],
})
export class EditReviewPage implements OnInit {

  updateReviewForm: FormGroup;
  id:number;
  review;

  constructor(private router: Router, public formbuilder: FormBuilder, public toastCtrl: ToastController, private reviewService: ReviewService, private activatedRoute: ActivatedRoute) {
    this.updateReviewForm = this.formbuilder.group({
      text: [null, [Validators.required, Validators.maxLength(140)]],
      film_rating: [3],
    });

    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    })
  }


  ngOnInit() {
    this.getReview();
  }

  logRatingChange(rating){
    console.log("changed rating: ",rating);
  }

  async openToast() {
    const toast = await this.toastCtrl.create({
      message: 'Review enviado',
      duration: 4000,
      position: 'middle',
      color: 'roxinho',
    });
    toast.present();
  }

  updateReview(){
    this.updateReviewForm.value.film_id = this.review.film_id;
    this.reviewService.updateReview(this.id, this.updateReviewForm.value).subscribe(
      (res) => {
        console.log('abaxaci', res);
        this.router.navigate(['/film/' + this.review.film_id]).then(()=> {window.location.reload()});
      }
    )
  }

  getReview(){
    this.reviewService.getReview(this.id).subscribe(
      (res) => {
        this.review = res[0];
        console.log('review', this.review);
      })
  }
}
