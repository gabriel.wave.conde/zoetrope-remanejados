import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FilmPageRoutingModule } from './film-routing.module';

import { FilmPage } from './film.page';
import { UserReviewComponent } from '../components/user-review/user-review.component';
import { AddComponent } from '../components/add/add.component';
import { EditPostComponent } from '../components/edit-post/edit-post.component';
import { DeletePostComponent } from '../components/delete-post/delete-post.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FilmPageRoutingModule
  ],
  declarations: [FilmPage, UserReviewComponent, AddComponent, EditPostComponent, DeletePostComponent]
})
export class FilmPageModule {}
