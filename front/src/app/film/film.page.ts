import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FilmService } from '../services/film/film.service';
import { ReviewService } from '../services/review/review.service';
import { UserService } from '../services/user/user.service'

@Component({
  selector: 'app-film',
  templateUrl: './film.page.html',
  styleUrls: ['./film.page.scss'],
})
export class FilmPage implements OnInit {
  id:number;
  film;
  reviews;
  user;

  constructor(private router: Router, private filmService: FilmService, private activatedRoute: ActivatedRoute, private reviewService: ReviewService, private userService: UserService) {
    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    })
  }


  ngOnInit() {
    this.showFilm(this.id);
    this.listFilmReviews(this.id);
    this.getDetails();
  }


  showFilm(id) {
    this.filmService.showFilm(id).subscribe(
      (res) => {
        this.film = res.film;
        console.log(res.film);
      },
      (err) => {
        console.log(err);
      })
    }

  listFilmReviews(id) {
    this.filmService.listFilmReviews(id).subscribe(
      (res) => {
        this.reviews = res.reverse();
        console.log(res);
      }
    )
  }

  go(){
    this.router.navigate(['/create-review/' + this.id])
  }

  getDetails(){
    if (localStorage.getItem('userToken')){
      this.userService.getDetails().subscribe(
        (res) => {
          this.user = res.user;
        }
      )
    }
  }

  deleteReview(id,i){
    this.reviewService.deleteReview(id).subscribe(
      (res) => {
        console.log(res);
        this.reviews.splice(i,1);
      }
    )
  }
}
