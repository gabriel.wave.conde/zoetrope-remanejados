import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-friendlist',
  templateUrl: './friendlist.page.html',
  styleUrls: ['./friendlist.page.scss'],
})
export class FriendlistPage implements OnInit {

  friendlist;
  id: number;

  constructor(private router: Router, private userService: UserService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    })
   }

   friendList(){
   this.userService.friendList().subscribe(
    (res) => {
      this.friendlist = res.friend_list;
      console.log(this.friendlist);
      
    }
  )
}

  ngOnInit() {

    this.friendList();
  }

  go(id){
    this.router.navigate(['/tabs/tab3/' + id]).then(()=> {window.location.reload()})
  }
}
