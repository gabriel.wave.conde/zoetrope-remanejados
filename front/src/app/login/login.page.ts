import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;

  constructor(public formbuilder: FormBuilder, public authService: AuthService, private router: Router) {
    this.loginForm = this.formbuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(15)]]
    });
  }

  go(){
    this.router.navigate(['/register'])
  }

  ngOnInit() {
  }

  login(form){
    this.authService.login(form.value).subscribe(
      (res) => {
        console.log(res);
        localStorage.setItem('userToken', res.token);
        this.router.navigate(['/tabs/tab1']).then(() =>{window.location.reload()});
      },
      (err) => {
        console.log(err.error.message);
      })
  }
}
