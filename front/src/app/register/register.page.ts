import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;

  constructor(public formbuilder: FormBuilder, public authService: AuthService, private router: Router, public toastCtrl: ToastController) {
    this.registerForm = this.formbuilder.group({
      email: [null, [Validators.email]],
      user_name: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
      password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(15)]],
      confirm_password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(15)]]
    });
  }

  go(){
    this.router.navigate(['/login'])
  }

  ngOnInit() {
  }

  register(form){
    console.log(form);
    this.authService.register(this.registerForm.value).subscribe(
      (res)=> {
        console.log(res);
        localStorage.setItem('userToken', res.success.token);
        this.router.navigate(['/tabs/tab1'])
      },
      (err)=> {
        console.log(err);
      }
    )
  }

  async openToast() {  
    const toast = await this.toastCtrl.create({  
      message: 'Usuário cadastrado com sucesso',   
      duration: 4000,
      position: 'bottom',
      color: 'roxinho',
    });  
    toast.present();  
  } 
}
