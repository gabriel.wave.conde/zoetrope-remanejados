import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CommentService {

  apiUrl: string = "http://localhost:8000/api/";

  constructor(public http: HttpClient) { }

  httpHeaders: any = {
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    }
  }

  createComment(form): Observable<any> {
    this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
    return this.http.post(this.apiUrl + 'comment', form, this.httpHeaders );
  }

  deleteComment(id): Observable<any>{
    this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
    return this.http.delete(this.apiUrl + 'comment/' + id, this.httpHeaders)
  }

  updateComment(id, form): Observable<any>{
    this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
    return this.http.put(this.apiUrl + 'comment/' + id, form, this.httpHeaders);
  }

  getComment(id): Observable<any> {
    return this.http.get(this.apiUrl + 'comment/' + id, this.httpHeaders);
  }
}
