import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  apiUrl: string = "http://localhost:8000/api/";

  constructor(public http: HttpClient) { }

  httpHeaders: any = {
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  }

  listFilms(): Observable<any>{
    return this.http.get(this.apiUrl + 'film', this.httpHeaders);
  }

  showFilm(id): Observable<any> {
    return this.http.get(this.apiUrl + 'film/' + id, this.httpHeaders);
  }

  listFilmReviews(id): Observable<any> {
    return this.http.get(this.apiUrl + 'film/reviews/' + id, this.httpHeaders);
  }

  search(form): Observable<any> {
    return this.http.put(this.apiUrl + 'search', form, this.httpHeaders);
  }
}
