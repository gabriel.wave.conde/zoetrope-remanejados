import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { identifierModuleUrl } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  apiUrl: string = "http://localhost:8000/api/";

  constructor(public http: HttpClient) { }

  httpHeaders: any = {
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  }
  getReview(id): Observable<any> {
    return this.http.get(this.apiUrl + 'review/' + id, this.httpHeaders);

  }

  listReviewComments(id): Observable<any> {
    return this.http.get(this.apiUrl + 'review/comments/' + id, this.httpHeaders);
  }

  createReview(form): Observable<any> {
    this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
    return this.http.post(this.apiUrl + 'review', form, this.httpHeaders)
  }

  likeReview(id): Observable<any>{
    this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
    return this.http.post(this.apiUrl + 'user/like_review/' + id, null, this.httpHeaders);
  }

  isLikedReview(id): Observable<any>{
    this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
    return this.http.get(this.apiUrl + 'is_liked_review/' + id, this.httpHeaders)
  }

  deleteReview(id): Observable<any>{
    this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
    return this.http.delete(this.apiUrl + 'review/' + id, this.httpHeaders)
  }

  updateReview(id, form): Observable<any>{
    this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
    return this.http.put(this.apiUrl + 'review/' + id, form, this.httpHeaders);
  }
}
