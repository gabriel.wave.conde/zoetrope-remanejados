import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

export class Movies{
  title: string;
  photo: string;
  genre: string;
  lenght: string;
  resume: string;
  director: string;
  release_date: string;
  year: string;
  age_rating: string;
  media_rating: string;
}

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  movies: Movies[];
  movies2: Movies[];
  movies3: Movies[];
  movies4: Movies[];
  movies5: Movies[];

  slideOpts = {slidesPerView: 3}



  constructor(private router: Router) {}

  go(){
    this.router.navigate(['/search'])
  }

  ngOnInit() {
    this.movies = [
    {
      title: 'Nasce Uma Estrela',
      photo: '../../assets/movies/nasce-uma-estrela-poster.jpg',
      genre: 'Drama, Musical, Romance',
      lenght: '2h 16min ',
      resume: 'Em Nasce Uma Estrela, Jackson Maine (Bradley Cooper) é um cantor no auge da fama. Um dia, após deixar uma apresentação, ele para em um bar para beber algo. É quando conhece Ally (Lady Gaga), uma insegura cantora que ganha a vida trabalhando em um restaurante. Jackson se encanta pela mulher e seu talento, decidindo acolhê-la debaixo de suas asas. Ao mesmo tempo em que Ally ascende ao estrelato, Jackson vive uma crise pessoal e profissional devido aos problemas com o álcool.',
      director: 'Bradley Cooper',
      release_date: '11/10/2018',
      year: '2018',
      age_rating: '16 anos',
      media_rating: '7.6',
    },
    {
      title: 'Coringa',
      photo: '../../assets/movies/joker-poster.jpg',
      genre: 'Drama',
      lenght: '2h 2min',
      resume: 'Em Coringa, Arthur Fleck (Joaquin Phoenix) trabalha como palhaço para uma agência de talentos e, toda semana, precisa comparecer a uma agente social, devido aos seus conhecidos problemas mentais. Após ser demitido, Fleck reage mal à gozação de três homens em pleno metrô e os mata. Os assassinatos iniciam um movimento popular contra a elite de Gotham City, da qual Thomas Wayne (Brett Cullen) é seu maior representante.',
      director: 'Todd Phillips',
      release_date: '03/10/2019',
      year: '2019',
      age_rating: '16 anos',
      media_rating: '8.4',
    },
    {
      title: 'Interestelar',
      photo: '../../assets/movies/interstellar-poster.png',
      genre: 'Ficção científica, Drama',
      lenght: '2h 49min',
      resume: 'Após ver a Terra consumindo boa parte de suas reservas naturais, um grupo de astronautas recebe a missão de verificar possíveis planetas para receberem a população mundial, possibilitando a continuação da espécie. Cooper (Matthew McConaughey) é chamado para liderar o grupo e aceita a missão sabendo que pode nunca mais ver os filhos. Ao lado de Brand (Anne Hathaway), Jenkins (Marlon Sanders) e Doyle (Wes Bentley), ele seguirá em busca de uma nova casa. Com o passar dos anos, sua filha Murph (Mackenzie Foy e Jessica Chastain) investirá numa própria jornada para também tentar salvar a população do planeta.',
      director: 'Christopher Nolan',
      release_date: '06/11/2014',
      year: '2014',
      age_rating: '10 anos',
      media_rating: '8.6',
    },
    {
      title: 'Parasita',
      photo: '../../assets/movies/parasite-poster.jpg',
      genre: 'Suspense',
      lenght: '2h 12min',
      resume: 'Em Parasita, toda a família de Ki-taek está desempregada, vivendo num porão sujo e apertado. Uma obra do acaso faz com que o filho adolescente da família comece a dar aulas de inglês à garota de uma família rica. Fascinados com a vida luxuosa destas pessoas, pai, mãe, filho e filha bolam um plano para se infiltrarem também na família burguesa, um a um. No entanto, os segredos e mentiras necessários à ascensão social custarão caro a todos.',
      director: 'Bong Joon Ho',
      release_date: '07/11/2019',
      year: '2019',
      age_rating: '16 anos',
      media_rating: '8.6',
    },
    {
      title: 'Bastardos Inglórios',
      photo: '../../assets/movies/bastardos-inglorios-poster.jpg',
      genre: 'Guerra, Ação',
      lenght: '2h 33min',
      resume: 'Em Bastardos Inglórios, na Segunda Guerra Mundial, a França está ocupada pelos nazistas. O tenente Aldo Raine (Brad Pitt) é o encarregado de reunir um pelotão de soldados de origem judaica, com o objetivo de realizar uma missão suicida contra os alemães. O objetivo é matar o maior número possível de nazistas, da forma mais cruel possível. Paralelamente Shosanna Dreyfuss (Mélanie Laurent) assiste a execução de sua família pelas mãos do coronel Hans Landa (Christoph Waltz), o que faz com que fuja para Paris. Lá ela se disfarça como operadora e dona de um cinema local, enquanto planeja um meio de se vingar',
      director: 'Quentin Tarantino',
      release_date: '09/10/2019',
      year: '2019',
      age_rating: '18 anos',
      media_rating: '8.3',
    },
  ]
  this.movies2 = [
    {
      title: 'Dois Irmãos: Uma Jornada Fantástica',
      photo: '../../assets/movies/onward-poster.jpg',
      genre: 'Animação, Fantasia',
      lenght: '1h 42min',
      resume: 'Em um local onde as coisas fantásticas parecem ficar cada vez mais distantes de tudo, dois irmãos elfos adolescentes embarcam em uma extraordinária jornada para tentar redescobrir a magia do mundo ao seu redor.',
      director: 'Dan Scanlon',
      release_date: '05/03/2020',
      year: '2020',
      age_rating: 'Livre',
      media_rating: '7.4/10',
    },
    {
      title: 'Soul',
      photo: '../../assets/movies/soul-poster.jpg',
      genre: 'Animação, Aventura, Família',
      lenght: '1h 40min',
      resume: 'Em Soul, duas perguntas se destacam: Você já se perguntou de onde vêm sua paixão, seus sonhos e seus interesses? O que é que faz de você... Você? A Pixar Animation Studios nos leva a uma jornada pelas ruas da cidade de Nova York e aos reinos cósmicos para descobrir respostas às perguntas mais importantes da vida.',
      director: 'Pete Docter',
      release_date: '25/12/2020',
      year: '2020',
      age_rating: 'Livre',
      media_rating: '8.1/10',
    },
    {
      title: 'Frozen II',
      photo: '../../assets/movies/frozen2-poster.jpg',
      genre: 'Animação, Musical',
      lenght: '1h 43min',
      resume: 'De volta à infância de Elsa e Anna, as duas irmãs descobrem uma história do pai, quando ainda era príncipe de Arendelle. Ele conta às meninas a história de uma visita à floresta dos elementos, onde um acontecimento inesperado teria provocado a separação dos habitantes da cidade com os quatro elementos fundamentais: ar, fogo, terra e água. Esta revelação ajudará Elsa a compreender a origem de seus poderes.',
      director: 'Chris Buck, Jennifer Lee',
      release_date: '02/01/2020',
      year: '2020',
      age_rating: 'Livre',
      media_rating: '6.9/10',
    },
    {
      title: 'Toy Story 4',
      photo: '../../assets/movies/toystory4-poster.jpeg',
      genre: 'Animação, Aventura, Família',
      lenght: '1h 40min',
      resume: 'Agora morando na casa da pequena Bonnie, Woody apresenta aos amigos o novo brinquedo construído por ela: Forky, baseado em um garfo de verdade. O novo posto de brinquedo não o agrada nem um pouco, o que faz com que Forky fuja de casa. Decidido a trazer de volta o atual brinquedo favorito de Bonnie, Woody parte em seu encalço e, no caminho, reencontra Bo Peep, que agora vive em um parque de diversões.',
      director: 'Josh Cooley',
      release_date: '20/06/2019',
      year: '2019',
      age_rating: 'Livre',
      media_rating: '7.8/10',
    },
    {
      title: 'Divertida Mente',
      photo: '../../assets/movies/divertida-mente-poster.jpg',
      genre: 'Animação, Comédia, Família',
      lenght: '1h 35min',
      resume: 'Riley é uma garota divertida de 11 anos de idade, que deve enfrentar mudanças importantes em sua vida quando seus pais decidem deixar a sua cidade natal, no estado de Minnesota, para viver em San Francisco. Dentro do cérebro de Riley, convivem várias emoções diferentes, como a Alegria, o Medo, a Raiva, o Nojinho e a Tristeza. A líder deles é Alegria, que se esforça bastante para fazer com que a vida de Riley seja sempre feliz. Entretanto, uma confusão na sala de controle faz com que ela e Tristeza sejam expelidas para fora do local. Agora, elas precisam percorrer as várias ilhas existentes nos pensamentos de Riley para que possam retornar à sala de controle - e, enquanto isto não acontece, a vida da garota muda radicalmente.',
      director: 'Pete Docter',
      release_date: '18/06/2015',
      year: '2015',
      age_rating: 'Livre',
      media_rating: '8.1/10',
    },
    {
      title: 'Viva: A Vida é uma Festa',
      photo: '../../assets/movies/coco-poster.jpg',
      genre: 'Animação, Fantasia, Família',
      lenght: '1h 45min',
      resume: 'Em Viva - A Vida é uma Festa, Miguel é um menino de 12 anos que quer muito ser um músico famoso, mas ele precisa lidar com sua família que desaprova seu sonho. Determinado a virar o jogo, ele acaba desencadeando uma série de eventos ligados a um mistério de 100 anos. A aventura, com inspiração no feriado mexicano do Dia dos Mortos, acaba gerando uma extraordinária reunião familiar.',
      director: 'Lee Unkrich, Adrian Molina',
      release_date: '04/01/2018',
      year: '2018',
      age_rating: 'Livre',
      media_rating: '8.4',
    },
  ]
  this.movies3 = [
    {
      title: 'Viúva Negra',
      photo: '../../assets/movies/viuva-negra-poster.jfif',
      genre: 'Ação, Espionagem, Aventura',
      lenght: '2h 13min',
      resume: 'Em Viúva Negra, após seu nascimento, Natasha Romanoff (Scarlett Johansson) é dada à KGB, que a prepara para se tornar sua agente definitiva. Quando a URSS rompe, o governo tenta matá-la enquanto a ação se move para a atual Nova York, onde ela trabalha como freelancer. Após aventuras com os Vingadores, ela retorna para seu país de origem e se une à antigos aliados para acabar com o programa governamental que a transformou em uma assassina.',
      director: 'Cate Shortland',
      release_date: '07/05/2021',
      year: '2021',
      age_rating: '',
      media_rating: '',
    },
    {
      title: 'Duna',
      photo: '../../assets/movies/duna-poster.jpeg',
      genre: 'Ficção científica, Drama',
      lenght: ' min',
      resume: 'Em um futuro distante, planetas são comandados por casas nobres que fazem parte de um império feudal intergalático. Paul Atreides (Timothée Chalamet) é um jovem homem cuja família toma controle do planeta deserto Arrakis, também conhecido como Duna. Sendo a única fonte da especiaria melange, a substância mais importante do cosmos, Arrakis se prova ser um planeta nem um pouco fácil de governar.',
      director: 'Denis Villeneuve',
      release_date: '01/10/2021',
      year: '2021',
      age_rating: '',
      media_rating: '',
    },
    {
      title: 'Um lugar silencioso 2',
      photo: '../../assets/movies/quiet-place-2-poster.jpg',
      genre: 'Suspense, Fantasia, Terror',
      lenght: '1h 45min',
      resume: 'Em Um Lugar Silencioso - Parte 2, logo após os acontecimentos mortais, a família Abbott precisa agora encarar o terror mundo afora, continuando a lutar para sobreviver em silêncio. Obrigados a se aventurar pelo desconhecido, eles rapidamente percebem que as criaturas que caçam pelo som não são as únicas ameaças que os observam pelo caminho de areia.',
      director: 'John Krasinski',
      release_date: '09/09/2021',
      year: '2021',
      age_rating: '',
      media_rating: '',
    },
    {
      title: 'Raya e o Último Dragão',
      photo: '../../assets/movies/raya-poster.jpg',
      genre: 'Animação, Fantasia, Aventura',
      lenght: '1h 30min',
      resume: 'Em Raya e o Último Dragão, Kumandra é um reino habitado por uma vasta e antiga civilização conhecida por ter passado gerações venerado os dragões, seus poderes e sua sabedoria. Porém, com as criaturas desaparecidas, a terra é tomada por uma força obscura. Quando uma guerreira chamada Raya, convencida de que a espécie não foi extinta, decide sair em busca do último dragão, sua aventura pode mudar o curso de todo o mundo.',
      director: 'Don Hall, Carlos Lopez Estrada, Paul Briggs',
      release_date: '04/03/2021',
      year: '2021',
      age_rating: 'Livre',
      media_rating: '',
    },
    {
      title: '007 - Sem Tempo Para Morrer',
      photo: '../../assets/movies/007-poster.jpg',
      genre: 'Ação, Suspense, Espionagem',
      lenght: '2h 43min',
      resume: 'Em 007 - Sem Tempo Para Morrer, o famoso espião James Bond (Daniel Craig) deixou o serviço e está vivendo de forma calma e pacífica na Jamaica. Como tudo que é bom dura pouco, quando o enigmático Safin (Rami Malek) aparece com uma tecnologia perigosa, seu amigo da CIA, Felix Leiter (Jeffrey Wright), pede ajuda. Então, o antigo 007 precisa abandonar seu novo modo de vida para ingressar na missão.',
      director: 'Cary Joji Fukunaga',
      release_date: '08/10/2021',
      year: '2021',
      age_rating: '',
      media_rating: '',
    },
    {
      title: 'Cruella',
      photo: '../../assets/movies/cruella-poster.jpg',
      genre: 'Comédia, Drama, Família',
      lenght: '',
      resume: 'Filme live-action sobre a rica e excêntrica Cruella De Vil, obcecada por capturar os adoráveis dálmatas. Sinopse oficial não divulgada.',
      director: 'Craig Gillespie',
      release_date: '03/06/2021',
      year: '2021',
      age_rating: '',
      media_rating: '',
    },
  ]
  this.movies4 = [
    {
      title: 'Vingadores: Ultimato',
      photo: '../../assets/movies/vingadores-poster.jpg',
      genre: 'Ação, Fantasia, Aventura',
      lenght: '3h 1min',
      resume: 'Em Vingadores: Ultimato, após Thanos eliminar metade das criaturas vivas em Vingadores: Guerra Infinita, os heróis precisam lidar com a dor da perda de amigos e seus entes queridos. Com Tony Stark (Robert Downey Jr.) vagando perdido no espaço sem água nem comida, o Capitão América/Steve Rogers (Chris Evans) e a Viúva Negra/Natasha Romanov (Scarlett Johansson) precisam liderar a resistência contra o titã louco.',
      director: 'Anthony Russo, Joe Russo',
      release_date: '25/04/2019',
      year: '2019',
      age_rating: '12 anos',
      media_rating: '8.4',
    },
    {
      title: 'Capitã Marvel',
      photo: '../../assets/movies/capita-marvel-poster.jpg',
      genre: 'Ação, Fantasia, Ficção científica',
      lenght: '2h 04min',
      resume: 'Em Capitã Marvel, Carol Danvers (Brie Larson) é uma ex-agente da Força Aérea norte-americana, que, sem se lembrar de sua vida na Terra, é recrutada pelos Kree para fazer parte de seu exército de elite. Inimiga declarada dos Skrull, ela acaba voltando ao seu planeta de origem para impedir uma invasão dos metaformos, e assim vai acabar descobrindo a verdade sobre si, com a ajuda do agente Nick Fury (Samuel L. Jackson) e da gata Goose.',
      director: 'Anna Boden, Ryan Fleck',
      release_date: '07/03/2019',
      year: '2019',
      age_rating: '12 anos',
      media_rating: '6.9',
    },
    {
      title: 'Mulher-Maravilha 1984',
      photo: '../../assets/movies/mulher-maravilha-1984-poster.jpg',
      genre: 'Ação, Aventura, Fantasia',
      lenght: '2h 31min',
      resume: 'Mulher-Maravilha 1984 acompanha Diana Prince/Mulher-Maravilha (Gal Gadot) em 1984, durante a Guerra Fria, entrando em conflito com dois grande inimigos - o empresário de mídia Maxwell Lord (Pedro Pascal) e a amiga que virou inimiga Barbara Minerva/Cheetah (Kristen Wiig) - enquanto se reúne com seu interesse amoroso Steve Trevor (Chris Pine).',
      director: 'Patty Jenkins',
      release_date: '17/12/2020',
      year: '2020',
      age_rating: '12 anos',
      media_rating: '5.4',
    },
    {
      title: 'Aves de Rapina: Arlequina e sua Emancipação Fantabulosa',
      photo: '../../assets/movies/arlequina-poster.jpg',
      genre: 'Ação, Aventura',
      lenght: '1h 49min',
      resume: 'Em Aves de Rapina - Arlequina e sua Emancipação Fantabulosa, Arlequina (Margot Robbie), Canário Negro (Jurnee Smollett-Bell), Caçadora (Mary Elizabeth Winstead), Cassandra Cain e a policial Renée Montoya (Rosie Perez) formam um grupo inusitado de heroínas. Quando um perigoso criminoso começa a causar destruição em Gotham, as cinco mulheres precisam se unir para defender a cidade.',
      director: 'Cathy Yan',
      release_date: '06/02/2020',
      year: '2020',
      age_rating: '16 anos',
      media_rating: '6.1',
    },
  ]
  this.movies5 = [
    {
      title: 'Django Livre',
      photo: '../../assets/movies/django-poster.jpg',
      genre: 'Faroeste',
      lenght: '2h 44min',
      resume: 'No sul dos Estados Unidos, o ex-escravo Django faz uma aliança inesperada com o caçador de recompensas Schultz para caçar os criminosos mais procurados do país e resgatar sua esposa de um fazendeiro que força seus escravos a participar de competições mortais.',
      director: 'Quentin Tarantino',
      release_date: '18/01/2013',
      year: '2013',
      age_rating: '16 anos',
      media_rating: '8.4',
    },
    {
      title: 'Mad Max: Estrada da Fúria',
      photo: '../../assets/movies/mad-max-poster.jpg',
      genre: 'Ação, Ficção científica',
      lenght: '2h 00min',
      resume: 'Após ser capturado por Immortan Joe, um guerreiro das estradas chamado Max (Tom Hardy) se vê no meio de uma guerra mortal, iniciada pela Imperatriz Furiosa (Charlize Theron) na tentativa se salvar um grupo de garotas. Também tentanto fugir, Max aceita ajudar Furiosa em sua luta contra Joe e se vê dividido entre mais uma vez seguir sozinho seu caminho ou ficar com o grupo.',
      director: 'George Miller',
      release_date: '14/05/2015',
      year: '2015',
      age_rating: '16 anos',
      media_rating: '8.1',
    },
    {
      title: 'A Origem',
      photo: '../../assets/movies/a-origem-poster.jpg',
      genre: 'Ficção científica, Suspense',
      lenght: '2h 28min',
      resume: 'Em um mundo onde é possível entrar na mente humana, Cobb (Leonardo DiCaprio) está entre os melhores na arte de roubar segredos valiosos do inconsciente, durante o estado de sono. Além disto ele é um fugitivo, pois está impedido de retornar aos Estados Unidos devido à morte de Mal (Marion Cotillard). Desesperado para rever seus filhos, Cobb aceita a ousada missão proposta por Saito (Ken Watanabe), um empresário japonês: entrar na mente de Richard Fischer (Cillian Murphy), o herdeiro de um império econômico, e plantar a ideia de desmembrá-lo. Para realizar este feito ele conta com a ajuda do parceiro Arthur (Joseph Gordon-Levitt), a inexperiente arquiteta de sonhos Ariadne (Ellen Page) e Eames (Tom Hardy), que consegue se disfarçar de forma precisa no mundo dos sonhos.',
      director: 'Christopher Nolan',
      release_date: '06/08/2010',
      year: '2010',
      age_rating: '14 anos',
      media_rating: '8.8',
    },
    {
      title: 'La La Land: Cantando Estações',
      photo: '../../assets/movies/la-la-land-poster.jpg',
      genre: 'Comédia Musical, Romance',
      lenght: '2h 08min',
      resume: 'Ao chegar em Los Angeles o pianista de jazz Sebastian (Ryan Gosling) conhece a atriz iniciante Mia (Emma Stone) e os dois se apaixonam perdidamente. Em busca de oportunidades para suas carreiras na competitiva cidade, os jovens tentam fazer o relacionamento amoroso dar certo enquanto perseguem fama e sucesso.',
      director: 'Damien Chazelle',
      release_date: '19/01/2017',
      year: '2017',
      age_rating: 'Livre',
      media_rating: '8.0',
    },
    {
      title: 'Estrelas Além Do Tempo',
      photo: '../../assets/movies/estrelas-poster.jpg',
      genre: 'Drama, Biografia',
      lenght: '2h 07min',
      resume: 'Durante a corrida espacial, três matemáticas negras Katherine Johnson (Taraji P. Henson), Dorothy Vaughn (Octavia Spencer) e Mary Jackson (Janelle Monáe) tentam se autoafirmar dentro da NASA, ainda muito racista e sexista. Enquanto lidam com o chefe (Kevin Costner) e o funcionário Paul (Jim Parsons), que tenta sabotá-las.',
      director: 'Theodore Melfi',
      release_date: '02/02/2017',
      year: '2017',
      age_rating: 'Livre',
      media_rating: '7.8',
    },
    {
      title: 'Jojo Rabbit',
      photo: '../../assets/movies/jojo-rabbit-poster.jpg',
      genre: 'Guerra, Drama, Comédia',
      lenght: '105 minutos',
      resume: 'Com apenas dez anos, o sonho do pequeno Jojo Betzler (Roman Griffin Davis) é se unir ao exército nazista, que está em combate no fim da Segunda Guerra Mundial. Sua mãe Rosie (Scarlett Johansson) então o envia ao acampamento do capitão Klenzendorf (Sam Rockwell), mas ele acaba voltando para casa e descobrindo que uma garota judia (Thomasin McKenzie) está escondida no local. Sem saber como lidar com a situação, Jojo pede constantemente a opinião de seu amigo imaginário Adolf (Taika Waititi). No entanto, o laço que cria com a menina parece ir de encontro a tudo em que acredita.',
      director: 'Taika Waititi',
      release_date: '06/02/2020',
      year: '2020',
      age_rating: '14 anos',
      media_rating: '7.9',
    },
  ]
}

}
