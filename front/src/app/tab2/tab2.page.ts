import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FilmService } from '../services/film/film.service';
import { Router } from '@angular/router';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page implements OnInit {

  searchForm: FormGroup;
  filterTerm: string;
  filmList = [];
  user;
  loggedUser;

  constructor(public filmService: FilmService, private router: Router, public formbuilder: FormBuilder, public userService: UserService) {
    this.searchForm = this.formbuilder.group({
      search: [null, [Validators.required]]
    })
  }

  ngOnInit() {
    this.getFilms();
    this.getDetails();
  }

  getFilms() {
    this.filmService.listFilms().subscribe(
      (res) => {
        this.filmList = res.film;
        console.log(this.filmList);
      })
  }

  search(){
    this.filmService.search(this.searchForm.value).subscribe(
      (res) => {
        this.filmList = res;
        console.log('bom dia ejcmigos', this.filmList);
      })
  }

  getDetails(){
    if (localStorage.getItem('userToken')){
      this.userService.getDetails().subscribe(
        (res) => {
          this.loggedUser = res.user;
          console.log('milao', this.loggedUser);
        }
      )
    }
  }
}

