import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user/user.service';

export class Movies{
  photo: string;
  rating: string;
}

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{

  movies: Movies[];

  slideOpts = {slidesPerView: 3}

  user;
  id: number;
  logado;
  userReviews;

  constructor(private router: Router, private userService: UserService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    })
  }

  go(){
    this.router.navigate(['/edit-profile/' + this.id])
  }

  go2(){
    this.router.navigate(['/friendlist/' + this.id])
  }

  showUser(id) {
    this.userService.showUser(id).subscribe(
      (res) => {
        this.user = res.user[0];
      }
    )
  }

  getDetails(){
    this.userService.getDetails().subscribe(
      (res) => {
        this.logado = res;
        console.log('frutinha', this.logado);

      }
    )
  }


  ngOnInit() {
    this.getUserReviews();

    this.showUser(this.id);

    this.getDetails();


    this.movies = [
      {
        photo: '../../assets/photos/The_Space_Between_Us_poster.jpg',
        rating: '4.7',
      },
      {
        photo: '../../assets/photos/A_Nova_Onda_do_Imperador_Pôster.jpg',
        rating: '5.0',
      },
      {
        photo: '../../assets/photos/clube-da-luta-poster.jpg',
        rating: '4.8',
      },
      {
        photo: '../../assets/photos/Joias-Brutas-poster.jpg',
        rating: '4.4',
      },
      {
        photo: '../../assets/photos/lady-bird-poster.jpg',
        rating: '2.2',
      },
    ]
  }

  getUserReviews() {
    this.userService.getUserReviews(this.id).subscribe(
      (res) => {
        this.userReviews = res.reviews.reverse()[0];
        console.log('amaom', this.userReviews);
      })
  }
}


