import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user/user.service'


@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {
feed;

constructor(private router: Router, private userService: UserService) {}

go(){
  this.router.navigate(['/list-info'])

}

ngOnInit() {
  this.getFeed();
}

getFeed(){
  this.userService.getFeed().subscribe(
    (res) => {
      this.feed = res.feed.reverse();
      console.log('morango', this.feed);
    })
  }
}
