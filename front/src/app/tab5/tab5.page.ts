import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


export class Movies{
  title: string;
  photo: string;
  genre: string;
  lenght: string;
  resume: string;
  director: string;
  release_date: string;
  year: string;
  age_rating: string;
  media_rating: string;
}
@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {
  movies: Movies[];

  constructor(private router: Router) {}

  go(){
    this.router.navigate(['/login'])
  }

  go2(){
    this.router.navigate(['/register'])
  }

 ngOnInit() {
    this.movies = [
      {
        title: 'Mulher-Maravilha 1984',
        photo: '../../assets/movies/mulher-maravilha-1984-poster.jpg',
        genre: 'Ação, Aventura, Fantasia',
        lenght: '2h 31min',
        resume: 'Mulher-Maravilha 1984 acompanha Diana Prince/Mulher-Maravilha (Gal Gadot) em 1984, durante a Guerra Fria, entrando em conflito com dois grande inimigos - o empresário de mídia Maxwell Lord (Pedro Pascal) e a amiga que virou inimiga Barbara Minerva/Cheetah (Kristen Wiig) - enquanto se reúne com seu interesse amoroso Steve Trevor (Chris Pine).',
        director: 'Patty Jenkins',
        release_date: '17/12/2020',
        year: '2020',
        age_rating: '12 anos',
        media_rating: '5.4',
      },
      {
        title: 'Dois Irmãos: Uma Jornada Fantástica',
        photo: '../../assets/movies/onward-poster.jpg',
        genre: 'Animação, Fantasia',
        lenght: '1h 42min',
        resume: 'Em um local onde as coisas fantásticas parecem ficar cada vez mais distantes de tudo, dois irmãos elfos adolescentes embarcam em uma extraordinária jornada para tentar redescobrir a magia do mundo ao seu redor.',
        director: 'Dan Scanlon',
        release_date: '05/03/2020',
        year: '2020',
        age_rating: 'Livre',
        media_rating: '7.4/10',
      },
      {
        title: 'Soul',
        photo: '../../assets/movies/soul-poster.jpg',
        genre: 'Animação, Aventura, Família',
        lenght: '1h 40min',
        resume: 'Em Soul, duas perguntas se destacam: Você já se perguntou de onde vêm sua paixão, seus sonhos e seus interesses? O que é que faz de você... Você? A Pixar Animation Studios nos leva a uma jornada pelas ruas da cidade de Nova York e aos reinos cósmicos para descobrir respostas às perguntas mais importantes da vida.',
        director: 'Pete Docter',
        release_date: '25/12/2020',
        year: '2020',
        age_rating: 'Livre',
        media_rating: '8.1/10',
      },
      {
        title: 'Parasita',
        photo: '../../assets/movies/parasite-poster.jpg',
        genre: 'Suspense',
        lenght: '2h 12min',
        resume: 'Em Parasita, toda a família de Ki-taek está desempregada, vivendo num porão sujo e apertado. Uma obra do acaso faz com que o filho adolescente da família comece a dar aulas de inglês à garota de uma família rica. Fascinados com a vida luxuosa destas pessoas, pai, mãe, filho e filha bolam um plano para se infiltrarem também na família burguesa, um a um. No entanto, os segredos e mentiras necessários à ascensão social custarão caro a todos.',
        director: 'Bong Joon Ho',
        release_date: '07/11/2019',
        year: '2019',
        age_rating: '16 anos',
        media_rating: '8.6',
      },
      {
        title: 'Bastardos Inglórios',
        photo: '../../assets/movies/bastardos-inglorios-poster.jpg',
        genre: 'Guerra, Ação',
        lenght: '2h 33min',
        resume: 'Em Bastardos Inglórios, na Segunda Guerra Mundial, a França está ocupada pelos nazistas. O tenente Aldo Raine (Brad Pitt) é o encarregado de reunir um pelotão de soldados de origem judaica, com o objetivo de realizar uma missão suicida contra os alemães. O objetivo é matar o maior número possível de nazistas, da forma mais cruel possível. Paralelamente Shosanna Dreyfuss (Mélanie Laurent) assiste a execução de sua família pelas mãos do coronel Hans Landa (Christoph Waltz), o que faz com que fuja para Paris. Lá ela se disfarça como operadora e dona de um cinema local, enquanto planeja um meio de se vingar',
        director: 'Quentin Tarantino',
        release_date: '09/10/2019',
        year: '2019',
        age_rating: '18 anos',
        media_rating: '8.3',
      },
      {
        title: 'Frozen II',
        photo: '../../assets/movies/frozen2-poster.jpg',
        genre: 'Animação, Musical',
        lenght: '1h 43min',
        resume: 'De volta à infância de Elsa e Anna, as duas irmãs descobrem uma história do pai, quando ainda era príncipe de Arendelle. Ele conta às meninas a história de uma visita à floresta dos elementos, onde um acontecimento inesperado teria provocado a separação dos habitantes da cidade com os quatro elementos fundamentais: ar, fogo, terra e água. Esta revelação ajudará Elsa a compreender a origem de seus poderes.',
        director: 'Chris Buck, Jennifer Lee',
        release_date: '02/01/2020',
        year: '2020',
        age_rating: 'Livre',
        media_rating: '6.9/10',
      },
      {
        title: 'Toy Story 4',
        photo: '../../assets/movies/toystory4-poster.jpeg',
        genre: 'Animação, Aventura, Família',
        lenght: '1h 40min',
        resume: 'Agora morando na casa da pequena Bonnie, Woody apresenta aos amigos o novo brinquedo construído por ela: Forky, baseado em um garfo de verdade. O novo posto de brinquedo não o agrada nem um pouco, o que faz com que Forky fuja de casa. Decidido a trazer de volta o atual brinquedo favorito de Bonnie, Woody parte em seu encalço e, no caminho, reencontra Bo Peep, que agora vive em um parque de diversões.',
        director: 'Josh Cooley',
        release_date: '20/06/2019',
        year: '2019',
        age_rating: 'Livre',
        media_rating: '7.8/10',
      },
      {
        title: 'Nasce Uma Estrela',
        photo: '../../assets/movies/nasce-uma-estrela-poster.jpg',
        genre: 'Drama, Musical, Romance',
        lenght: '2h 16min ',
        resume: 'Em Nasce Uma Estrela, Jackson Maine (Bradley Cooper) é um cantor no auge da fama. Um dia, após deixar uma apresentação, ele para em um bar para beber algo. É quando conhece Ally (Lady Gaga), uma insegura cantora que ganha a vida trabalhando em um restaurante. Jackson se encanta pela mulher e seu talento, decidindo acolhê-la debaixo de suas asas. Ao mesmo tempo em que Ally ascende ao estrelato, Jackson vive uma crise pessoal e profissional devido aos problemas com o álcool.',
        director: 'Bradley Cooper',
        release_date: '11/10/2018',
        year: '2018',
        age_rating: '16 anos',
        media_rating: '7.6',
      },
      {
        title: 'Divertida Mente',
        photo: '../../assets/movies/divertida-mente-poster.jpg',
        genre: 'Animação, Comédia, Família',
        lenght: '1h 35min',
        resume: 'Riley é uma garota divertida de 11 anos de idade, que deve enfrentar mudanças importantes em sua vida quando seus pais decidem deixar a sua cidade natal, no estado de Minnesota, para viver em San Francisco. Dentro do cérebro de Riley, convivem várias emoções diferentes, como a Alegria, o Medo, a Raiva, o Nojinho e a Tristeza. A líder deles é Alegria, que se esforça bastante para fazer com que a vida de Riley seja sempre feliz. Entretanto, uma confusão na sala de controle faz com que ela e Tristeza sejam expelidas para fora do local. Agora, elas precisam percorrer as várias ilhas existentes nos pensamentos de Riley para que possam retornar à sala de controle - e, enquanto isto não acontece, a vida da garota muda radicalmente.',
        director: 'Pete Docter',
        release_date: '18/06/2015',
        year: '2015',
        age_rating: 'Livre',
        media_rating: '8.1/10',
      },
      {
        title: 'Viva: A Vida é uma Festa',
        photo: '../../assets/movies/coco-poster.jpg',
        genre: 'Animação, Fantasia, Família',
        lenght: '1h 45min',
        resume: 'Em Viva - A Vida é uma Festa, Miguel é um menino de 12 anos que quer muito ser um músico famoso, mas ele precisa lidar com sua família que desaprova seu sonho. Determinado a virar o jogo, ele acaba desencadeando uma série de eventos ligados a um mistério de 100 anos. A aventura, com inspiração no feriado mexicano do Dia dos Mortos, acaba gerando uma extraordinária reunião familiar.',
        director: 'Lee Unkrich, Adrian Molina',
        release_date: '04/01/2018',
        year: '2018',
        age_rating: 'Livre',
        media_rating: '8.4',
      },
      {
        title: 'Coringa',
        photo: '../../assets/movies/joker-poster.jpg',
        genre: 'Drama',
        lenght: '2h 2min',
        resume: 'Em Coringa, Arthur Fleck (Joaquin Phoenix) trabalha como palhaço para uma agência de talentos e, toda semana, precisa comparecer a uma agente social, devido aos seus conhecidos problemas mentais. Após ser demitido, Fleck reage mal à gozação de três homens em pleno metrô e os mata. Os assassinatos iniciam um movimento popular contra a elite de Gotham City, da qual Thomas Wayne (Brett Cullen) é seu maior representante.',
        director: 'Todd Phillips',
        release_date: '03/10/2019',
        year: '2019',
        age_rating: '16 anos',
        media_rating: '8.4',
      },
      {
        title: 'Interestelar',
        photo: '../../assets/movies/interstellar-poster.png',
        genre: 'Ficção científica, Drama',
        lenght: '2h 49min',
        resume: 'Após ver a Terra consumindo boa parte de suas reservas naturais, um grupo de astronautas recebe a missão de verificar possíveis planetas para receberem a população mundial, possibilitando a continuação da espécie. Cooper (Matthew McConaughey) é chamado para liderar o grupo e aceita a missão sabendo que pode nunca mais ver os filhos. Ao lado de Brand (Anne Hathaway), Jenkins (Marlon Sanders) e Doyle (Wes Bentley), ele seguirá em busca de uma nova casa. Com o passar dos anos, sua filha Murph (Mackenzie Foy e Jessica Chastain) investirá numa própria jornada para também tentar salvar a população do planeta.',
        director: 'Christopher Nolan',
        release_date: '06/11/2014',
        year: '2014',
        age_rating: '10 anos',
        media_rating: '8.6',
      },
      {
        title: 'Django Livre',
        photo: '../../assets/movies/django-poster.jpg',
        genre: 'Faroeste',
        lenght: '2h 44min',
        resume: 'No sul dos Estados Unidos, o ex-escravo Django faz uma aliança inesperada com o caçador de recompensas Schultz para caçar os criminosos mais procurados do país e resgatar sua esposa de um fazendeiro que força seus escravos a participar de competições mortais.',
        director: 'Quentin Tarantino',
        release_date: '18/01/2013',
        year: '2013',
        age_rating: '16 anos',
        media_rating: '8.4',
      },
      {
        title: 'Mad Max: Estrada da Fúria',
        photo: '../../assets/movies/mad-max-poster.jpg',
        genre: 'Ação, Ficção científica',
        lenght: '2h 00min',
        resume: 'Após ser capturado por Immortan Joe, um guerreiro das estradas chamado Max (Tom Hardy) se vê no meio de uma guerra mortal, iniciada pela Imperatriz Furiosa (Charlize Theron) na tentativa se salvar um grupo de garotas. Também tentanto fugir, Max aceita ajudar Furiosa em sua luta contra Joe e se vê dividido entre mais uma vez seguir sozinho seu caminho ou ficar com o grupo.',
        director: 'George Miller',
        release_date: '14/05/2015',
        year: '2015',
        age_rating: '16 anos',
        media_rating: '8.1',
      },
      {
        title: 'Capitã Marvel',
        photo: '../../assets/movies/capita-marvel-poster.jpg',
        genre: 'Ação, Fantasia, Ficção científica',
        lenght: '2h 04min',
        resume: 'Em Capitã Marvel, Carol Danvers (Brie Larson) é uma ex-agente da Força Aérea norte-americana, que, sem se lembrar de sua vida na Terra, é recrutada pelos Kree para fazer parte de seu exército de elite. Inimiga declarada dos Skrull, ela acaba voltando ao seu planeta de origem para impedir uma invasão dos metaformos, e assim vai acabar descobrindo a verdade sobre si, com a ajuda do agente Nick Fury (Samuel L. Jackson) e da gata Goose.',
        director: 'Anna Boden, Ryan Fleck',
        release_date: '07/03/2019',
        year: '2019',
        age_rating: '12 anos',
        media_rating: '6.9',
      },
    ]
  }
}


